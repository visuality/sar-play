//
//  SARplayDemoConnector.swift
//  SARplay
//
//  Created by Michele Longhi on 05/03/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import Foundation
import CoreLocation
import RealmSwift
import UIKit


class SARplayDemoConnector: SARplayConnector{
    
    override func getAvailableZones(completion handler: ((zones: [SARZone]?) -> ())?) {
        
        // TODO: Generare delle zone demo se non presenti nel db
        
        let demoZones: [[String: AnyObject]] = [
            [
                "title": "Sondrio",
                "id_zone": "001",
                "area": [
                    
                    ["latitude": 46.152679, "longitude": 9.811537],
                    ["latitude": 46.172697, "longitude": 10.061070]
                    
                ],
                "points": [
                    ["id_point":"1111", "latitude": 46.183008, "longitude": 9.883677, "elevation": 310.0, "radius": 0.0, "velocity":(-1.4)],
                    ["id_point":"1112", "latitude": 46.168944, "longitude":  9.870524, "elevation": 306.0, "radius": 0.0, "velocity":(-0.9)],
                    ["id_point":"1113", "latitude": 46.153753, "longitude": 9.857542, "elevation": 312.0, "radius": 0.0, "velocity":(-0.3)],
                    ["id_point":"1114", "latitude": 46.180706, "longitude": 9.947531, "elevation": 301.0, "radius": 0.0, "velocity":1],
                    ["id_point":"1115", "latitude": 46.169268, "longitude": 9.991170, "elevation": 309.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1116", "latitude": 46.175898, "longitude": 9.856620, "elevation": 311.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1117", "latitude": 46.158303, "longitude": 9.838767, "elevation": 302.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1118", "latitude": 46.156757, "longitude": 9.890094, "elevation": 305.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1119", "latitude": 46.170905, "longitude": 9.887690, "elevation": 311.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1120", "latitude": 46.167577, "longitude": 9.868808, "elevation": 306.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1121", "latitude": 46.174828, "longitude": 9.872927, "elevation": 313.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1122", "latitude": 46.169268, "longitude": 9.991170, "elevation": 320.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1123", "latitude": 46.172926, "longitude": 9.885459, "elevation": 316.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1124", "latitude": 46.166388, "longitude": 9.845977, "elevation": 321.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1125", "latitude": 46.178989, "longitude": 9.875331, "elevation": 305.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1126", "latitude": 46.161989, "longitude": 9.881854, "elevation": 304.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1127", "latitude": 46.179583, "longitude": 9.871382, "elevation": 300.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1128", "latitude": 46.169954, "longitude": 9.901252, "elevation": 314.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1129", "latitude": 46.159849, "longitude": 9.843058, "elevation": 319.0, "radius": 0.0, "velocity":0.1],
                    ["id_point":"1130", "latitude": 46.165080, "longitude": 9.901080, "elevation": 322.0, "radius": 0.0, "velocity":0.1]
                    
                ]
            ],
            [
                "title": "Milano",
                "id_zone": "002",
                "area": [
                    ["latitude": 45.083150, "longitude":  8.417271],
                    ["latitude": 45.704914, "longitude":  9.676235]
                ],
                "points": [
                    ["id_point":"22221", "latitude": 45.499061, "longitude":  9.100140, "elevation": 120.0, "radius": 0.0, "velocity":(-0.2)],
                    ["id_point":"22222", "latitude": 45.528518,  "longitude": 9.201364, "elevation": 130.0, "radius": 0.0, "velocity":(-0.3)],
                    ["id_point":"22223", "latitude": 45.426340,  "longitude": 9.248970, "elevation": 110.0, "radius": 0.0, "velocity":0.4],
                    ["id_point":"22224", "latitude": 45.425376, "longitude":  9.150951, "elevation": 121.0, "radius": 0.0, "velocity":0.5],
                    ["id_point":"22225", "latitude": 45.468973,  "longitude": 9.181507, "elevation": 119.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22226", "latitude": 45.515599,  "longitude": 9.155066, "elevation": 116.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22227", "latitude": 45.495869,  "longitude": 9.203818, "elevation": 123.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22228", "latitude": 45.487204,  "longitude": 9.100134, "elevation": 129.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22229", "latitude": 45.478539,  "longitude": 9.192145, "elevation": 102.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22230", "latitude": 45.448199,  "longitude": 9.161932, "elevation": 106.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22231", "latitude": 45.416396,  "longitude": 9.231970, "elevation": 102.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22232", "latitude": 45.469872,  "longitude": 9.259436, "elevation": 99.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22233", "latitude": 45.440491,  "longitude": 9.131720, "elevation": 106.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22234", "latitude": 45.500200,  "longitude": 9.171545, "elevation": 130.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22235", "latitude": 45.460722,  "longitude": 9.214117, "elevation": 120.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22236", "latitude": 45.457350,  "longitude": 9.174978, "elevation": 124.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22237", "latitude": 45.494906,  "longitude": 9.202444, "elevation": 111.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22238", "latitude": 45.401935,  "longitude": 9.183218, "elevation": 119.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22239", "latitude": 45.482390,  "longitude": 9.078161, "elevation": 126.0, "radius": 0.0, "velocity":1],
                    ["id_point":"22240", "latitude": 45.522816,  "longitude": 9.120047, "elevation": 118.0, "radius": 0.0, "velocity":1]
                    
                ]
            ],
            
            [
                "title": "Roma",
                "id_zone": "003",
                "area": [
                    ["latitude": 41.577081141707936, "longitude": 12.16963525869214],
                    ["latitude": 42.17826987751136, "longitude": 12.77082399449556]
                    
                ],
                "points": [
                    ["id_point":"33331", "latitude": 41.929401,  "longitude": 12.376689, "elevation": 0.0, "radius": 0.0, "velocity":0.7],
                    ["id_point":"33332", "latitude": 41.981996,  "longitude": 12.498225, "elevation": 0.0, "radius": 0.0, "velocity":(-0.8)],
                    ["id_point":"33333", "latitude": 41.901379,  "longitude": 12.595912, "elevation": 0.0, "radius": 0.0, "velocity":0.9],
                    ["id_point":"33334", "latitude": 41.818964,  "longitude": 12.544917, "elevation": 0.0, "radius": 0.0, "velocity":1],
                    ["id_point":"33335", "latitude": 41.884431,  "longitude": 12.392482, "elevation": 0.0, "radius": 0.0, "velocity":1.4],
                    
                ]
                
            ],
            
            
            [
                "title": "Otranto",
                "id_zone": "004",
                "area": [
                    ["latitude": 40.143293141707936, "longitude": 18.46381325869214],
                    ["latitude": 40.16094287751136, "longitude": 18.51546999449556]
                    
                ],
                "points": [
                    ["id_point":"44441", "latitude": 40.150382,  "longitude": 18.481306, "elevation": 2.0, "radius": 0.0, "velocity":0.7],
                    ["id_point":"44442", "latitude": 40.143428,  "longitude": 18.487400, "elevation": 0.0, "radius": 0.0, "velocity":(-0.8)],
                    ["id_point":"44443", "latitude": 40.147128,  "longitude": 18.482400, "elevation": 3.0, "radius": 0.0, "velocity":(-1.5)],
                    ["id_point":"44444", "latitude": 40.152679,  "longitude": 18.476929, "elevation": 0.0, "radius": 0.0, "velocity":1.0],
                    ["id_point":"44445", "latitude": 40.152482,  "longitude": 18.489546, "elevation": 9.0, "radius": 0.0, "velocity":(-1.2)],
                    ["id_point":"44446", "latitude": 40.147364,  "longitude": 18.497400, "elevation": 2.0, "radius": 0.0, "velocity":(-0.1)],
                    ["id_point":"44447", "latitude": 40.140000,  "longitude": 18.487000, "elevation": 0.5, "radius": 0.0, "velocity":1.0],
                    ["id_point":"44448", "latitude": 40.155500,  "longitude": 18.490754, "elevation": 0.9, "radius": 0.0, "velocity":(1.2)],
                    ["id_point":"44449", "latitude": 40.148939,  "longitude": 18.482400, "elevation": 9.0, "radius": 0.0, "velocity":0.8],
                    ["id_point":"44450", "latitude": 40.155893,  "longitude": 18.479161, "elevation": 5.0, "radius": 0.0, "velocity":(0.2)]
                    
                ]
                
            ]
            
        ]
        
        var zones = [SARZone]()
        
        let deformationsTest = List<SARDeformation>()
        
    
        
        
        let realm = Realm.getInstance()
        
        for demoZone in demoZones {

            
            let title = demoZone["title"] as! String
            
            let id_zone = demoZone["id_zone"] as! String
            
            let demoArea = demoZone["area"] as! [[String:Double]]
            
            let lowerCorner = demoArea[0]
            let upperCorner = demoArea[1]
            
            
            let lowerCornerCoordinate = CLLocationCoordinate2D(latitude: lowerCorner["latitude"]!, longitude: lowerCorner["longitude"]!)
            
            let upperCornerCoordinate = CLLocationCoordinate2D(latitude: upperCorner["latitude"]!, longitude: upperCorner["longitude"]!)
            
            
            let zone = SARZone.zoneWithTitle(title,
                                             withId: id_zone,
                                             lowerLeftCornerCoordinate: lowerCornerCoordinate,
                                             upperRightCornerCoordinate: upperCornerCoordinate,
                                             inRealm: realm)
            
            
            guard let demoPoints = demoZone["points"] as? [[String:AnyObject]] else {continue}
            
            let realm = Realm.getInstance()
            
            let points = List<SARPoint>()
            
            for demoPoint in demoPoints {
                
                guard let pointId: String = demoPoint["id_point"] as? String else {continue}
                guard let latitude: Double = demoPoint["latitude"] as? Double else {continue}
                guard let longitude: Double = demoPoint["longitude"] as? Double else {continue}
                guard let elevation: Double = demoPoint["elevation"] as? Double else {continue}
                guard let velocity: Double = demoPoint["velocity"] as? Double else {continue}
                
                let sarPoint = SARPoint.pointWithId(pointId,
                                                    coordinate: CLLocationCoordinate2D(latitude: latitude,longitude: longitude),
                                                    elevation: elevation,
                                                    velocity: velocity,
                                                    pointDeformations: List<SARDeformation>(),
                                                    inRealm: realm)
                
                
                points.append(sarPoint)
                
               
                
                
            }
            
            zone.points.appendContentsOf(points)

            try! realm.write {
                
                realm.add(zone)
            }
            
        }
        
        handler?(zones: zones.count > 0 ? zones : nil)
    
    }
}