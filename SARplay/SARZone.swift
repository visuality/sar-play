//
//  SARZone.swift
//  SARplay
//
//  Created by Roberto Cabassi on 22/05/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation


class SARZone: Object {
    
    // MARK: Properties

    dynamic var title: String = ""
    dynamic var zoneId: String = ""
    dynamic var downloaded: Bool = false
    dynamic var lastSync: NSDate?
    
    private dynamic var lowerLeftCornerLatitude: Double = 0.0
    private dynamic var lowerLeftCornerLongitude: Double = 0.0
    private dynamic var upperRightCornerLatitude: Double = 0.0
    private dynamic var upperRightCornerLongitude: Double = 0.0

    private dynamic var bookmarked_: Bool = false
    
    let points = List<SARPoint>()
    
    // MARK: Ignored properties
    
    var lowerLeftCorner: CLLocationCoordinate2D {
        
        return CLLocationCoordinate2D(latitude: lowerLeftCornerLatitude, longitude: lowerLeftCornerLongitude)
    }
    
    var upperRightCorner: CLLocationCoordinate2D {
        
        return CLLocationCoordinate2D(latitude: upperRightCornerLatitude, longitude: upperRightCornerLongitude)
    }
    
    var isBookmarked: Bool {
        
        return bookmarked_
    }
    
    func bookmark(isBookmarked: Bool) {
        
        let realm = Realm.getInstance()
        
        try! realm.write {
            
            self.bookmarked_ = isBookmarked
        }
        
    }
    
    
    // MARK: Realm overrides
    
    
    // MARK: Methods
   
     static func zoneWithTitle(title: String,
                            withId id: String,
                            lowerLeftCornerCoordinate lowerLeft: CLLocationCoordinate2D,
                            upperRightCornerCoordinate upperRight: CLLocationCoordinate2D,
                            inRealm realm: Realm
                            ) -> SARZone {
        
        let zone = SARZone()
        
        zone.title = title
        zone.zoneId = id
        zone.lastSync = NSDate()
        zone.downloaded = true
        
        zone.lowerLeftCornerLatitude = lowerLeft.latitude
        zone.lowerLeftCornerLongitude = lowerLeft.longitude
        zone.upperRightCornerLatitude = upperRight.latitude
        zone.upperRightCornerLongitude = upperRight.longitude
        
        
        return zone
    }
    
    
    static func zoneWithTitle(title: String) -> SARZone? {
        
        let realm = Realm.getInstance()
        
        return realm.objects(SARZone).filter("title = '\(title)'").first
    }
    
    static func allZonesInRealm() -> Results<SARZone> {
        
        let realm = Realm.getInstance()
        
        return realm.objects(SARZone)
    }
    
    static func allBookmarkedZones() -> Results<SARZone> {
        
        let realm = Realm.getInstance()
        
        return realm.objects(SARZone).filter("bookmarked_ = true")
    }

    
}
