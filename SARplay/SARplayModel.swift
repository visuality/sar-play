//
//  SARplayModel.swift
//  SARplay
//
//  Created by Roberto Cabassi on 22/05/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

class SARplayModel {
    
    static let sharedInstance = SARplayModel()
    
    func startupWithCompletion(handler: (success: Bool)->()) {
        
        
        
         //self.connector = SARplayDemoConnector(model: self)
        
        self.connector = SARplayJSONConnector(model: self)
        
        getAvailableZones { (zones) -> () in
            handler(success: true)
        }
    }
    
    var connector: SARplayConnector?
    
    // MARK: - Main entities access
    
    func getAvailableZones(completion handler: ((zones: [SARZone]?)->())?) {
        
        // Get zones from local data
        
        
        let zones = Array(SARZone.allZonesInRealm())
        
        
        
        handler?(zones:zones)
        
        
        connector?.getAvailableZones(completion: { (zones) -> () in
            
            print("zone updated")
            
            handler?(zones: zones)
            
            
            print(SARZone.allZonesInRealm())
        })
        
    }
    
    
}