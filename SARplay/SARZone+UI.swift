//
//  SARPoint+UI.swift
//  SARplay
//
//  Created by Michele Longhi on 09/05/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

extension SARZone {
    
    var mapRect: MKMapRect {
        
        let width = upperRightCorner.latitude - lowerLeftCorner.latitude
        let height = upperRightCorner.longitude - lowerLeftCorner.longitude
        
        var rect = MKMapRectMake(lowerLeftCorner.latitude,
                                 lowerLeftCorner.longitude,
                                 width,
                                 height
        )
        
        
        if rect.size.width < 0 {
            
            rect.size.width += 180.0
        }
        
        if rect.size.height < 0 {
            
            rect.size.height += 360.0
        }
        
        let finalRect = rect
        
        return finalRect
    }
    
    
    class func zoneContainingLocation(location: CLLocation) -> SARZone? {
        
        let point = MKMapPointMake(location.coordinate.latitude, location.coordinate.longitude)
        
        let zones = SARZone.allZonesInRealm()
        
        for zone in zones {
            
            if MKMapRectContainsPoint(zone .mapRect, point) {
                
                return zone
            }
        }
        
        return nil
    }
    
}


