//
//  Realm.swift
//  SARplay
//
//  Created by Roberto Cabassi on 22/05/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    
    static func getInstance() -> Realm {
        
        var realmInstance: Realm?
        
        do {
            realmInstance  = try Realm()
            
        }
        catch {
            print("Error in realm init \(error)")
        }
        
        // Se non crea realm è come se non ci fosse il contesto, quindi è ragionevole che la app vada in crash
        
        return realmInstance!
        
    }
    
}