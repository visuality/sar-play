//
//  SARplayJSONConnector.swift
//  SARplay
//
//  Created by Michele Longhi on 05/03/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import Foundation
import CoreLocation
import RealmSwift


class SARplayJSONConnector: SARplayConnector {
    
    // MARK: Ovverride main method

    var realm: Realm!
    
    override func getAvailableZones(completion handler: ( (zones: [SARZone]?) -> () )?) {
        
        var json: [String: AnyObject]?
        
        json = nil
        
        // Start background thread
    
        json = self.loadJSON()
        
        dispatch_async(dispatch_get_global_queue(0, 0)) {
            
            self.realm = Realm.getInstance()
            
            var zones = [SARZone]()
            
            // TODO: Eliminare la zona se esiste
            
            let zone = SARZone.zoneWithTitle("Roma",
                                              withId: "csk_roma",
                                              lowerLeftCornerCoordinate: CLLocationCoordinate2D(latitude: 41.577081141707936, longitude: 12.16963525869214),
                                              upperRightCornerCoordinate: CLLocationCoordinate2D(latitude: 42.17826987751136, longitude: 12.77082399449556),
                                              inRealm: self.realm
                                    )
            
            zones.append(zone)
            
            let points = List<SARPoint>()
            
            guard let finalJson = json,
                features = finalJson["features"] as? [AnyObject] else { return }
            
            
            var index = 0
            
            for feature in features {
                
                let point = self.sarPointForFeature(feature, zone: zone)
                
                if let point = point {
                    

                    points.append(point)
                }
                
                print("Importing point \(index)")
                
                index += 1
            }
            
            zone.points.appendContentsOf(points)
            
            try! self.realm.write {
                
                self.realm.add(zone)
            }
            
            let callingPoints = zones[0].points
            print("calling handler with \(callingPoints.count)")
            
            
            handler?(zones: zones.count > 0 ? zones : nil)
        }
    }

    // MARK: Logic methods
    
    private func sarPointForFeature(feature: AnyObject, zone: SARZone) -> SARPoint? {
        
        guard let pointId = feature["id"] as? String,
            properties = feature["properties"] as? [String: AnyObject],
            latitude = properties["lat"] as? Double,
            longitude = properties["lon"] as? Double,
            elevation = properties["elevation"] as? Double,
            velocity = properties["velocity"] as? Double
            
            else {
                print("Error in creating sarPointForFeature")
                return nil
        }
        
        let deformations = sarDeformationsForProperties(properties)
        
        let sarPoint = SARPoint.pointWithId(pointId,
                                            coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude),
                                            elevation: elevation,
                                            velocity: velocity,
                                            pointDeformations: deformations,
                                            inRealm: realm

        )
        
        
            
        sarPoint.pointDeformations.appendContentsOf(deformations)
        
        
        return sarPoint
    }
    
    private func sarDeformationsForProperties(properties: AnyObject) -> List<SARDeformation> {
        
        let deformations = List<SARDeformation>()
        
       // Creo l' NSDictionary delle proprietà
        
        guard let propertyDictionary: NSDictionary = properties as? NSDictionary else {print("error converting to nsdict");return deformations}
        
        // Creo la regexp
        
        var failableRegex: NSRegularExpression?
        
        do {
            
            failableRegex = try NSRegularExpression(pattern: "[0-9]+", options: .CaseInsensitive)
        }
        catch {
            
            print("Error in creating regex \(error)")
        }
        
        guard let regex = failableRegex else {
            print("Regex not initialized")
            return deformations
        }
        
        // Itero sul dict
        
        for (key, value) in propertyDictionary {
            
            if let keyNsString = key as? NSString,
                keyString = key as? String {
                
                let matches = regex.matchesInString(keyString, options: [], range: NSRange(location: 0, length: keyString.utf16.count))
                
                let matchesArray = matches.map {
                    keyNsString.substringWithRange($0.range)
                }
                
                if matchesArray.count == 2 {
                    
                    // Allora è un campo valido
                    guard let year  = Int(matchesArray[0]),
                        hours = Int(matchesArray[1]) else {return deformations}
                    
                    let deformationDate = dateWithYear(year, hoursFromBeginningOfYear: hours)
                    
                    let deformationValue = value as! Double
                    
                    let deformation = SARDeformation.createDeformation(deformationDate,
                                                                       value: deformationValue,
                                                                       inRealm: realm)
                    
                    
                    deformations.append(deformation)
                    
                }
                
                
            }
        }
        
        
        return deformations
    }
    
    private func loadJSON() -> [String: AnyObject]? {
        
        do {
            
            guard let url = NSBundle.mainBundle().URLForResource("zoneTest4", withExtension: "json")
                
                else {
                    print("Error for json")
                    return nil
            }
            
            
            let data = try NSData(contentsOfURL: url, options: .DataReadingUncached)
            
            
            let object = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
            
            if let dict = object as? [String: AnyObject] {
                
                return dict
            }
        }
        catch {
            
            print("Error in parsing json file: \(error)")
        }
        
        
        
        return nil
    }
    
    
}

func dateWithYear(year: Int, hoursFromBeginningOfYear hours: Int) -> NSDate {
    
    let dateComponent = NSDateComponents()
    
    dateComponent.year = year
    
    let calendar = NSCalendar.currentCalendar()
    
    let date = calendar.dateFromComponents(dateComponent)
    
    let interval: NSTimeInterval = Double(hours) * 60 * 60
    
    return date!.dateByAddingTimeInterval(interval)
    
}

extension NSRange {
    func rangeForString(str: String) -> Range<String.Index>? {
        guard location != NSNotFound else { return nil }
        return str.startIndex.advancedBy(location) ..< str.startIndex.advancedBy(location + length)
    }
}
