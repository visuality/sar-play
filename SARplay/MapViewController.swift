//
//  ViewController.swift
//  SARplay
//
//  Created by Michele Longhi on 24/02/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var TypeLocation: UIBarButtonItem!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var setBookmark: UIBarButtonItem!
    @IBOutlet weak var mapStyleSelector: UISegmentedControl!
    
    // MARK: - Properties
    
    var alert: UIAlertController!
    
    var userPosition: CLLocation?
    
    var pointVelocity: Double?
    
    var sarPointForPopover: SARPoint!
  
    var locationManager: CLLocationManager?
    var currentZone: SARZone? {
        
        willSet {
            
            print("CLEAR")
            clearData()
        }
        
        didSet {
            
            print("RELOAD")
            reloadData()
        }
    }
    
    @IBAction func touchDown(sender: AnyObject) {
        
        if currentZone?.isBookmarked == false{
            currentZone?.bookmark(true)
            setBookmark.image = UIImage(named: "bookmark.png")
        }
        else{
            currentZone?.bookmark(false)
            setBookmark.image = UIImage(named: "not-bookmark.png")
            
        }
    }
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Gesture recognizer per tap su SARPoint
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MapViewController.handleMapTap(_:)))
        gestureRecognizer.numberOfTapsRequired = 1
        gestureRecognizer.cancelsTouchesInView = false
        
        mapView.addGestureRecognizer(gestureRecognizer)
        
        
        // Creazione alert Automatic
        alert = UIAlertController(title: "Errore caricamento zona!", message: "Zona richiesta non presente", preferredStyle:UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title:"Ok", style:UIAlertActionStyle.Default, handler: nil);
        alert.addAction(action);
    
        setupUI()
        setupLocationManager()
    }
    
    func handleMapTap(tap: UIGestureRecognizer) {
        
        let tapPoint = tap.locationInView(mapView)
        
        let tapCoordinate = mapView.convertPoint(tapPoint, toCoordinateFromView: mapView)
        
        let mkPoint = MKMapPointForCoordinate(tapCoordinate)
        
        for overlay in mapView.overlays {
            
            if let circle = overlay as? MKCircle {
                
                let renderer = mapView.rendererForOverlay(circle)
                
                if let circleRenderer = renderer as? MKCircleRenderer {
                    
                    let circlePoint = circleRenderer.pointForMapPoint(mkPoint)
                    
                    if CGPathContainsPoint(circleRenderer.path, nil, circlePoint, true) {
                        
                        showPopoverForCircle(circle)
                        return
                    }
                }
                
            }
            
        }
        
    }
    
    func showPopoverForCircle(circle: MKCircle) {
        
        guard let currentZone = self.currentZone else {return}
        
        let points = currentZone.points
        
        for point in points {
            
            if circle.coordinate.latitude == point.coordinate.latitude && circle.coordinate.longitude == point.coordinate.longitude {
                
                mapView.removeAnnotations(mapView.annotations)
                
                self.sarPointForPopover = point
                mapView.addAnnotation(circle)
                
                mapView.selectAnnotation(circle, animated: true)
                
                
            }
        }
    }
    
    // Funzione per creare il popover con dentro lo XIB
    func pointInfoView(sender: MKAnnotationView) {
        
        let pointInfoViewController = PointInfoViewController(nibName: "PointInfoViewController", bundle: nil)
    
        pointInfoViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
        
        pointInfoViewController.popoverPresentationController?.permittedArrowDirections = .Any
        
        pointInfoViewController.sarPoint = sarPointForPopover
        
        presentViewController(pointInfoViewController, animated: true, completion: nil)
    
        
        let popoverPresentationController = pointInfoViewController.popoverPresentationController
        
        
        popoverPresentationController?.sourceView = sender
        
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
       let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "infoAnnotation")

        annotationView.canShowCallout = true
        
        // Apre lo XIB come popover
        pointInfoView(annotationView)
        
        return annotationView
        
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
    
        // TODO: Far uscire lo xib sulla mappa
        print("Show callouts")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Delegates
    
    // MARK: Location management
    
    func locationManager(manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {return}
        
        if self.currentZone == nil {
            
            if let zone = SARZone.zoneContainingLocation(location) {
                
                self.currentZone = zone
                
            }
        }
        
        userPosition = location
        
    }
    
    // MARK: - Map View
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        if let overlay = overlay as? SarPointOverlay, velocity = overlay.point?.velocity {
            
            let circleRenderer = MKCircleRenderer(circle: overlay)
            
            let color = colorForVelocity(velocity)
            
            circleRenderer.strokeColor = color
            circleRenderer.alpha = 0.5
            
            
            return circleRenderer
        }
        else {
            
            return MKOverlayRenderer(overlay: overlay)
        }
    }
    
    
    
    func colorForVelocity(value: Double) -> UIColor {
        
        // Colori secondo le specifiche CNR
        
        let viola = (1.07143...Double.infinity)
        let blu = (0.64286...1.07143)
        let azzurro = (0.21429...0.64286)
        let verde = ((-0.21429)...0.21429)
        let giallo = ((-0.64286)...(-0.21429))
        let arancio = ((-1.07143)...(-0.64286))
        let rosso = ((-Double.infinity)...(-1.07143))
        
        
        if viola.contains(value) {
            
            return UIColor.purpleColor()
        }
        
        if blu.contains(value) {
            
            return UIColor.blueColor()
        }
        
        if azzurro.contains(value) {
            
            return UIColor.cyanColor()
        }
        
        if verde.contains(value) {
            
            return UIColor.greenColor()
        }
        
        if giallo.contains(value) {
            
            return UIColor.yellowColor()
        }
        
        if arancio.contains(value) {
            
            return UIColor.orangeColor()
        }
        
        if rosso.contains(value) {
            
            return UIColor.redColor()
        }
        
        return UIColor.blackColor()
        
    }
    
    
    // MARK: - Methods
    
    func clearData() {
        
        self.mapView.removeOverlays(self.mapView.overlays)
    }
    
    func reloadData() {
        
        guard let zone = self.currentZone else {return}
        let points = zone.points
        
        
        print("ZONA")
        print(zone.title)
        
        print("ZONA SCARICATA: \(zone)")
        
        // Spostamento della mappa
        
        let lowerLeftCorner = MKMapPointForCoordinate(zone.lowerLeftCorner)
        let upperRightCorner = MKMapPointForCoordinate(zone.upperRightCorner)
        
        let zoomMapRect = MKMapRectMake(lowerLeftCorner.x, upperRightCorner.y, upperRightCorner.x - lowerLeftCorner.x, lowerLeftCorner.y - upperRightCorner.y);
        
        self.mapView.setVisibleMapRect(zoomMapRect, animated: true)
    
        
        for point in points {
            
            print("Punto(\(point.id) [\(point.latitude), \(point.longitude)])")
            
            pointVelocity = point.velocity
            
            
            dispatch_async(dispatch_get_main_queue(), {
                
                let overlay = SarPointOverlay.sarPointOverlayWithPoint(point)

                
                self.mapView.addOverlay(overlay)
            })
        }
        
        
        if zone.isBookmarked == true {
            setBookmark.image = UIImage(named: "bookmark.png")
        }
        else{
            setBookmark.image = UIImage(named: "not-bookmark.png")
        }
        
    }
    
    func setupUI() {
        
        self.mapView.delegate = self
    }
    
    func setupLocationManager() {
        
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        
        self.locationManager?.requestWhenInUseAuthorization()
        self.locationManager?.startUpdatingLocation()
    }
    
    func manualType() {
        
        print("MANUAL TYPE")
        
        TypeLocation.image = UIImage(named:"automatico")
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let newController = segue.destinationViewController as? ZonesViewController {
            
            newController.mapView = self
            TypeLocation.image = UIImage(named:"automatico")
        }
        
        
        if let newController = segue.destinationViewController as? BookmarksViewController {
            
            newController.mapView = self
            TypeLocation.image = UIImage(named:"automatico")
        }
}
    
    
    
    func automaticType()
    {
        // TODO: capire come gestire il bottone quando e' in modalita GPS
        
        print("AUTOMATIC TYPE")
        
        TypeLocation.image = UIImage(named:"manuale")
        
        if let userPosition = self.userPosition {
            
            let span = MKCoordinateSpanMake(0.5, 0.5)
            let region = MKCoordinateRegion(center: userPosition.coordinate, span: span)
            mapView.setRegion(region, animated:true)
            
            
            currentZone = SARZone.zoneContainingLocation(userPosition)
            print("settint zone \(currentZone)")
            
            if currentZone == nil{
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
        }
        
        
    }
    
    
    // MARK: - Actions
    
    @IBAction func PressTypeLocation(sender: AnyObject) {
        
        
        if let immagine = TypeLocation.image {
            
            if immagine.isEqual(UIImage(named: "automatico")){
                
                automaticType()
                
            }
                
            else {
                
                manualType()
            }
            
        }
        
    }
    
    
    // Map style
    
    @IBAction func changeMapStyle(sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
            
        case 1:
            self.mapView.mapType = .Satellite
            
        default:
            self.mapView.mapType = .Standard
        }
    }
    
}

