//
//  SARDeformation.swift
//  SARplay
//
//  Created by Roberto Cabassi on 22/05/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import Foundation
import RealmSwift

class SARDeformation: Object {
    
    // MARK: Properties
    
    dynamic var date: NSDate?
    dynamic var value: Double = 0.0
    
    //let point = LinkingObjects(fromType: SARPoint.self, property: "pointDeformations")
    
    // MARK: Methods
    
    static func createDeformation(
        date: NSDate,
        value: Double,
        inRealm realm: Realm) -> SARDeformation {
        
        let deformation = SARDeformation()
        
        deformation.date = date
        deformation.value = value
        
        
        return deformation
    }
    
}
