//
//  PointInfoViewController.swift
//  SARplay
//
//  Created by Roberto Cabassi on 27/05/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import UIKit
import ScrollableGraphView

class PointInfoViewController: UIViewController {

    var sarPoint: SARPoint!

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var graphV: UIView!
    @IBOutlet weak var longitudeLabel: UILabel!
    
    @IBOutlet weak var velocityLabel: UILabel!
    @IBOutlet weak var elevationLabel: UILabel!
    
   var graphView = ScrollableGraphView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print(sarPoint)
        
        guard let sarPoint = self.sarPoint else {return}
        
        idLabel.text = sarPoint.id
        latitudeLabel.text = "\(sarPoint.latitude)"
        longitudeLabel.text = "\(sarPoint.longitude)"
        velocityLabel.text = "\(sarPoint.velocity)"
        elevationLabel.text = "\(sarPoint.elevation)"
        
        self.preferredContentSize = CGSizeMake(450, 400)
        
        // Creazione Grafico
        graphView = ScrollableGraphView(frame: graphV.frame)
        
        // Posizionamento del Grafico
        graphView.frame = CGRectMake(0, 0, graphV.frame.size.width, graphV.frame.size.height)
        
        graphView.backgroundFillColor = UIColor(red:0.75, green:0.75, blue:0.75, alpha:1.0)
//        graphView.layer.cornerRadius = 7
        
        graphView.rangeMin = -1.5
        graphView.rangeMax = 1.5
        
        graphView.lineWidth = 0.5
        graphView.lineColor = UIColor.redColor()
        graphView.lineStyle = ScrollableGraphViewLineStyle.Smooth
        graphView.shouldFill = true
        graphView.fillType = ScrollableGraphViewFillType.Gradient
        graphView.fillColor = UIColor.blackColor().colorWithAlphaComponent(0.8)
        graphView.fillGradientType = ScrollableGraphViewGradientType.Linear
        graphView.fillGradientStartColor = UIColor.brownColor().colorWithAlphaComponent(0.8)
        graphView.fillGradientEndColor = UIColor.orangeColor().colorWithAlphaComponent(0.6)
        graphView.dataPointSpacing = 80
        graphView.dataPointSize = 0.5
        graphView.dataPointFillColor = UIColor.whiteColor()
        
        graphView.referenceLineLabelFont = UIFont.boldSystemFontOfSize(8)
        graphView.referenceLineColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        graphView.referenceLineLabelColor = UIColor.whiteColor()
        graphView.dataPointLabelColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)

        let graphData = array(forPoint: sarPoint)
        
        print(graphData)
        
        graphView.setData(graphData.data, withLabels: graphData.labels)
        
        self.graphV.addSubview(graphView)

    }

    func array(forPoint point: SARPoint) -> (data: [Double], labels: [String]) {
    
        
        var data = [Double]()
        var labels = [String]()
        
        let calendar = NSCalendar.currentCalendar()
        
        
        // sortArray
        
        let sortedArray = point.pointDeformations.sort { (element1, element2) -> Bool in
            
            if let second1 = element1.date?.timeIntervalSince1970, second2 = element2.date?.timeIntervalSince1970{
                
                let milliDate1 = second1*1000
                let milliDate2 = second2*1000
                
                return  milliDate1 < milliDate2
            }
            
            return false
           
        }
        
        print("Sorted array:")
        print(sortedArray)
        
        
        // Creazione array per graphView
        
        for element in sortedArray{
            
            data.append(element.value)
            
            if let date = element.date{
                
                let dc = calendar.components([.Year, .Month, .Day], fromDate: date)
                
                let description = "\(dc.year)/\(dc.month)/\(dc.day)"
                
                labels.append(description)
                
            }
        }
        
        return (data, labels)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
