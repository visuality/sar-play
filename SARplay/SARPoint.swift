//
//  SARPoint.swift
//  SARplay
//
//  Created by Roberto Cabassi on 22/05/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation


class SARPoint: Object {
    
    // MARK: Properties
    
    dynamic var latitude: Double = 0.0
    dynamic var longitude: Double = 0.0
    dynamic var elevation: Double = 0.0
    dynamic var velocity: Double = 0.0
    dynamic var id: String = ""
    
    //let pointZone = LinkingObjects(fromType: SARZone.self, property: "points")
    let pointDeformations = List<SARDeformation>()

    
    // MARK: Methods
    
    static func pointWithId(id: String,
                            coordinate: CLLocationCoordinate2D,
                            elevation: Double,
                            velocity: Double,
                            pointDeformations deformations: List<SARDeformation>,
                            inRealm realm: Realm,
                            createIfNeeded: Bool = true) -> SARPoint {
        
        // Rimossa per problemi di prestazioni la ricerca di punto esistente
        //        let request = NSFetchRequest(entityName: "SARPoint")
        //        request.predicate = NSPredicate(format: "id == %@", id)
        //
        //        let points = (try? context.executeFetchRequest(request)) as? [SARPoint]
        //
        //        if let point = points?.first {
        //            return point
        //        }
        
        
        let point = SARPoint()
        
        point.id = id
        point.latitude = coordinate.latitude
        point.longitude = coordinate.longitude
        point.elevation = elevation
        point.velocity = velocity
    
        point.pointDeformations.appendContentsOf(deformations)
        
        
        return point
    }
    
}
