//
//  SarPointOverlay.swift
//  SARplay
//
//  Created by Roberto Cabassi on 07/06/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import UIKit
import MapKit

class SarPointOverlay: MKCircle {

    var point: SARPoint?
    
    class func sarPointOverlayWithPoint(point: SARPoint) -> SarPointOverlay {
        
        let overlay = SarPointOverlay(centerCoordinate: point.pointLocation, radius: 16)
        overlay.point = point
        
        return overlay
    }
    
}
