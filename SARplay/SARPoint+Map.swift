//
//  SARPoint+Map.swift
//  SARplay
//
//  Created by Michele Longhi on 04/03/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

extension SARPoint: MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D {
        
        return CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
    }
  
    
    var pointLocation: CLLocationCoordinate2D {
        
        return CLLocationCoordinate2DMake(self.latitude, self.longitude)
    }
}