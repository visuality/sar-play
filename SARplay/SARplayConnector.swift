//
//  SARplayConnector.swift
//  SARplay
//
//  Created by Michele Longhi on 24/02/16.
//  Copyright © 2016 Visuality srl. All rights reserved.
//

import Foundation
import RealmSwift

class SARplayConnector {
    
    weak var model: SARplayModel?
    
    init(model: SARplayModel? = nil) {
        
        self.model = model
    }
    
    // MARK: - Main methods
    
    func getAvailableZones(completion handler: ((zones: [SARZone]?)->() )?) {

        // TODO: Get available zones
        
        handler?(zones: nil)
    }
    
    func downloadZone(zone: SARZone, completion handler:(percent: Float, completed: Bool)->()) {
        
        // TODO: Download points from zone (partial download allowed)
        
        return handler(percent: 1.0, completed: true)
    }
}